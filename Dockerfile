FROM consol/centos-xfce-vnc:latest

# Switch to root user to install additional software
USER 0

# Install software

## vscode
RUN rpm --import https://packages.microsoft.com/keys/microsoft.asc
ADD ./files/vscode.repo /etc/yum.repos.d/
RUN yum install -y code 

## node.js
RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash - &&\
    yum install -y nodejs 

## python 
RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm &&\
    yum install -y python36u python36u-libs python36u-devel python36u-pip

# Switch back to default user
USER 1000

# vscode extensions
RUN code --install-extension ms-python.python &&\
    code --install-extension ms-azuretools.vscode-docker &&\
    code --install-extension hookyqr.beautify &&\
    code --install-extension gerane.theme-batman

# Set Theme for flavour 
ADD ./files/wallpaper.png /headless/.config
ADD themes /usr/share/themes
ADD xfce4 /headless/.config/xfce4/xfconf/xfce-perchannel-xml







